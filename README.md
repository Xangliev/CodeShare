This is the code for the Final Project of Semantic Web (Group 21 - Brandon Cchung & Rick Vergunst)
* Because of technical difficulties in conjunction with stardog, we had to revert our .ttl file to .owl to make it work.

Installation Guide:

1. Download the contents of this map as a ZIP. 
2. Open the contents of the ZIP file.
3. Run & create 2 databases in Stardog (localhost:5820): 
         <br > * One should be called "Eindprojectu" with the settings: Reasoning type SL, Reasoning approximate OFF and SameAs reasoning FULL
         <br > * The other should be called "Eindprojectuu" with the settings: Reasoning type SL, Reasoning approximate ON and SameAs reasoning FULL
4. Load the FinalProjectGroup21.owl file into both databases.
5. Run the scripts.py file (located in the FinalProjectGroup21 map) as python in your command shell.
6. Open localhost:5000 in your browser. 
