$('#link1').on('click', function(e){
	
	var query = $('#query1').text();
	var endpoint = 'http://localhost:5820/Eindprojectu/query';
	var format = 'JSON';
	var reason = 'true';
	
	$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);
		
		try {
			var vars = json.head.vars;
		
			var ul = $('<ul></ul>');
			ul.addClass('list-group');
		
			$.each(json.results.bindings, function(index,value){
				var li = $('<li></li>');
				li.addClass('list-group-item');
			
				$.each(vars, function(index, v){
					var v_type = value[v]['type'];
					var v_value = value[v]['value'];

					var myRegexp = /http:\/\/dbpedia.org\/resource\/(.*)/;
					var match = myRegexp.exec(v_value);
					li.append(v_value);
					li.append('<br/>');
					
				});
				ul.append(li);
			
			});
			
			$('#linktarget1').html(ul);
		} catch(err) {
			$('#linktarget1').html('Something went wrong!');
			console.log(query);
		}
		

		
	});
	
});

$('#link2').on('click', function(e){
	
	var query = $('#query1').text();
	var endpoint = 'http://localhost:5820/Eindprojectu/query';
	var format = 'JSON';
	var reason = 'false';
	
	$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);
		
		try {
			var vars = json.head.vars;
		
			var ul = $('<ul></ul>');
			ul.addClass('list-group');
		
			$.each(json.results.bindings, function(index,value){
				var li = $('<li></li>');
				li.addClass('list-group-item');
			
				$.each(vars, function(index, v){
					var v_type = value[v]['type'];
					var v_value = value[v]['value'];
				
					li.append('<strong>'+v+'</strong><br/>');
				
					// If the value is a URI, create a hyperlink
					if (v_type == 'uri') {
						var a = $('<a></a>');
						a.attr('href',v_value);
						a.text(v_value);
						li.append(a);
					// Else we're just showing the value.
					} else {
						li.append(v_value);
					}
					li.append('<br/>');
					
				});
				ul.append(li);
			
			});
			
			$('#linktarget2').html(ul);
		} catch(err) {
			$('#linktarget2').html('Something went wrong!');
		}
		

		
	});
	
});

function formLoad(){
	var query = $('#query1').text();
	var endpoint = 'http://localhost:5820/Eindprojectu/query';
	var format = 'JSON';
	var reason = 'true';

	$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);

	try {
			var vars = json.head.vars;

	var select = $('<select name = "Actor"></select>');
	select.addClass('form-control');

	$.each(json.results.bindings, function(index,value){
	var option = $('<option></option>');
	$.each(vars, function(index, v){
					var v_type = value[v]['type'];
					var v_value = value[v]['value'];

					var myRegexp = /http:\/\/dbpedia.org\/resource\/(.*)/;
					var match = myRegexp.exec(v_value);
					option.append(match[1]);
					option.append('<br/>');
					
				});
	select.append(option);
});

	$('#linktarget1').html(select);
	} catch(err) {
			$('#linktarget1').html('Something went wrong!');
		}

});
var query = $('#query2').text();
var endpoint = 'http://localhost:5820/Eindprojectuu/query';

$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);

	try {
			var vars = json.head.vars;

	var select = $('<select name = "Distri"></select>');
	select.addClass('form-control');

	$.each(json.results.bindings, function(index,value){
	var option = $('<option></option>');
	$.each(vars, function(index, v){
					var v_type = value[v]['type'];
					var v_value = value[v]['value'];

					var myRegexp = /http:\/\/dbpedia.org\/page\/(.*)/;
					var match = myRegexp.exec(v_value);
					option.append(match[1]);
					option.append('<br/>');
					
				});
	select.append(option);
});

	$('#linktarget2').html(select);
	} catch(err) {
			$('#linktarget2').html('Something went wrong!');
		}

});

var query = $('#query3').text();
var endpoint = 'http://localhost:5820/Eindprojectu/query';

$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);

	try {
			var vars = json.head.vars;

	var select = $('<select name = "Movie"></select>');
	select.addClass('form-control');

	$.each(json.results.bindings, function(index,value){
	var option = $('<option></option>');
	$.each(vars, function(index, v){
					var v_type = value[v]['type'];
					var v_value = value[v]['value'];

					var myRegexp = /http:\/\/dbpedia.org\/page\/(.*)/;
					var match = myRegexp.exec(v_value);
					//option.append(match[1]);
					option.append(v_value);
					option.append('<br/>');
					
				});
	select.append(option);
});

	$('#linktarget3').html(select);
	} catch(err) {
			$('#linktarget3').html('Something went wrong!');
		}

});

};

function formLoad2() {
	var query = $('#query4').text();
	var endpoint = 'http://localhost:5820/Eindprojectuu/query';
	var format = 'JSON';
	var reason = 'true';

$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);

	try {
			var vars = json.head.vars;

	var select = $('<select name = "Artist"></select>');
	select.addClass('form-control');

	$.each(json.results.bindings, function(index,value){
	var option = $('<option></option>');
	$.each(vars, function(index, v){
					var v_type = value[v]['type'];
					var v_value = value[v]['value'];

					option.append(v_value)
					option.append('</br>')
					
				});
	select.append(option);
});

	$('#linktarget1').html(select);
	} catch(err) {
			$('#linktarget1').html('Something went wrong!');
		}

});

var endpoint = 'http://localhost:5820/Eindprojectu/query';

var query = $('#query5').text();

$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);

	try {
			var vars = json.head.vars;

	var select = $('<select name = "Song"></select>');
	select.addClass('form-control');

	$.each(json.results.bindings, function(index,value){
	var option = $('<option></option>');
	$.each(vars, function(index, v){
					var v_type = value[v]['type'];
					var v_value = value[v]['value'];

					var myRegexp = /http:\/\/www.semanticweb.org\/finalproject#(.*)/;
					var match = myRegexp.exec(v_value);
					if (match) {
					option.append(match[1]);
					option.append('<br/>');
				}
					
				});
	select.append(option);
});

	$('#linktarget2').html(select);
	} catch(err) {
			$('#linktarget2').html('Something went wrong!');
		}

});

var query = $('#query6').text();

$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);

	try {
			var vars = json.head.vars;

	var select = $('<select name = "Genre"></select>');
	select.addClass('form-control');

	$.each(json.results.bindings, function(index,value){
	var option = $('<option></option>');
	$.each(vars, function(index, v){
					var v_type = value[v]['type'];
					var v_value = value[v]['value'];

					var myRegexp = /http:\/\/www.semanticweb.org\/finalproject#(.*)/;
					var match = myRegexp.exec(v_value);
					option.append(v_value);
					option.append('<br/>');
					
				});
	select.append(option);
});

	$('#linktarget3').html(select);
	} catch(err) {
			$('#linktarget3').html('Something went wrong!');
		}

});

}

function formLoad3() {
	var query = $('#query7').text();
	var endpoint = 'http://localhost:5820/Eindprojectu/query';
	var format = 'JSON';
	var reason = 'true';

$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);

	try {
			var vars = json.head.vars;

	var select = $('<select name = "Game"></select>');
	select.addClass('form-control');

	$.each(json.results.bindings, function(index,value){
	var option = $('<option></option>');
	$.each(vars, function(index, v){
					var v_type = value[v]['type'];
					var v_value = value[v]['value'];

					var myRegexp = /http:\/\/www.semanticweb.org\/finalproject#(.*)/;
					var match = myRegexp.exec(v_value);

					option.append(match[1])
					option.append('</br>')
					
				});
	select.append(option);
});

	$('#linktarget1').html(select);
	} catch(err) {
			$('#linktarget1').html('Something went wrong!');
		}

});

var query = $('#query8').text();

$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);

	try {
			var vars = json.head.vars;

	var select = $('<select name = "GameGenre"></select>');
	select.addClass('form-control');

	$.each(json.results.bindings, function(index,value){
	var option = $('<option></option>');
	$.each(vars, function(index, v){
					var v_type = value[v]['type'];
					var v_value = value[v]['value'];

					var myRegexp = /http:\/\/www.semanticweb.org\/finalproject#(.*)/;
					var match = myRegexp.exec(v_value);
					if (match) {
					option.append(match[1]);
					option.append('<br/>');
				}
					
				});
	select.append(option);
});

	$('#linktarget2').html(select);
	} catch(err) {
			$('#linktarget2').html('Something went wrong!');
		}

});

var query = $('#query9').text();

$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);

	try {
			var vars = json.head.vars;

	var select = $('<select name = "GameType"></select>');
	select.addClass('form-control');

	$.each(json.results.bindings, function(index,value){
	var option = $('<option></option>');
	$.each(vars, function(index, v){
					var v_type = value[v]['type'];
					var v_value = value[v]['value'];

					var myRegexp = /http:\/\/www.semanticweb.org\/finalproject#(.*)/;
					var match = myRegexp.exec(v_value);
					if (match) {
					option.append(match[1]);
					option.append('<br/>');
				}
					
				});
	select.append(option);
});

	$('#linktarget3').html(select);
	} catch(err) {
			$('#linktarget3').html('Something went wrong!');
		}

});

}

function searchActor(form) {
	var Actor = form.Actor.value;
	var query = 'PREFIX dbr: <http://dbpedia.org/resource/> \nPREFIX : <http://www.semanticweb.org/finalproject#> \n PREFIX purl: <http://purl.org/vocab/relationship/> \n SELECT DISTINCT ?x \nWHERE { \n '
	+'{dbr:'+ Actor +' :isMaleActorIn ?x} \n UNION {dbr:'+ Actor +' purl:colleagueOf ?x }\n UNION {dbr:'+ Actor +' :isVoiceActorIn ?x } \n UNION {dbr:'+ Actor +' purl:isActressIn ?x }\n}';
	var endpoint = 'http://localhost:5820/Eindprojectu/query';
	var format = 'JSON';
	var reason = 'true';
	var Actor2 = 'http://dbpedia.org/resource/' + Actor;
	console.log(typeof(query));

	$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);
		
		try {
			var vars = json.head.vars;
		
			var ul = $('<ul style = "list-style:none;"></ul>');
			ul.addClass('list-group');
		
			$.each(json.results.bindings, function(index,value){
				var li = $('<li></li>');
				li.addClass('list-group-item');
			
				$.each(vars, function(index, v){
					var v_value = value[v]['value'];

					//var myRegexp = /http:\/\/www.semanticweb.org\/finalproject#(.*)/;
					//var match = myRegexp.exec(v_value);
					li.append('<a href = "#" onClick="description(\''+Actor2+'\',\''+v_value+'\')">'+v_value+'</a>');
				}	
				);
				ul.append(li);
			
			});

			$('#linktarget4').html(ul);
		} catch(err) {
			$('#linktarget4').html('Something went wrong!');
		}
})
};

function searchDistribution(form) {
	var Distri = form.Distri.value;
	var query = 'PREFIX dbp: <http://dbpedia.org/page/> \n PREFIX : <http://www.semanticweb.org/finalproject#>  \nSELECT DISTINCT ?x \nWHERE { \n dbp:'+Distri+' :distributorOf ?x}';
	var endpoint = 'http://localhost:5820/Eindprojectu/query';
	var format = 'JSON';
	var reason = 'true';
	var Distri2 = 'http://dbpedia.org/page/' + Distri;
	console.log(query);

	$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);
		
		try {
			var vars = json.head.vars;
		
			var ul = $('<ul style = "list-style:none;"></ul>');
			ul.addClass('list-group');
		
			$.each(json.results.bindings, function(index,value){
				var li = $('<li></li>');
				li.addClass('list-group-item');
			
				$.each(vars, function(index, v){
					var v_value = value[v]['value'];

					//var myRegexp = /http:\/\/www.semanticweb.org\/finalproject#(.*)/;
					//var match = myRegexp.exec(v_value);
					li.append('<a href = "#" onClick="description(\''+Distri2+'\',\''+v_value+'\')">'+v_value+'</a>');
				}	
				);
				ul.append(li);
			
			});

			$('#linktarget4').html(ul);
		} catch(err) {
			$('#linktarget4').html('Something went wrong!');
		}
})
};

function searchMovie(form) {
	var Movie = form.Movie.value;
	var query = 'PREFIX dbp: <http://dbpedia.org/page/> \nPREFIX : <http://www.semanticweb.org/finalproject#> \n PREFIX mov: <http://www.movieontology.org/2009/10/01/movieontology.owl#> \nSELECT DISTINCT ?x \nWHERE { \n '
	+'{<'+ Movie +'> :hasVibe ?y .\n ?x :hasVibe ?y . \n FILTER (?x != <'+Movie+'>)}\n UNION {<'+Movie+'> mov:hasActor ?x} \n UNION {<'+Movie+'> mov:hasDirector ?x} \n}';
	var endpoint = 'http://localhost:5820/Eindprojectu/query';
	var format = 'JSON';
	var reason = 'true';
	var Movie2 = Movie;
	console.log(query);

	$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);
		
		try {
			var vars = json.head.vars;
		
			var ul = $('<ul style = "list-style:none;"></ul>');
			ul.addClass('list-group');
		
			$.each(json.results.bindings, function(index,value){
				var li = $('<li></li>');
				li.addClass('list-group-item');
			
				$.each(vars, function(index, v){
					var v_value = value[v]['value'];
					console.log(v_value);

					//var myRegexp = /http:\/\/www.semanticweb.org\/finalproject#(.*)/;
					//var match = myRegexp.exec(v_value);
					li.append('<a href = "#" onClick="description(\''+Movie2+'\',\''+v_value+'\')">'+v_value+'</a>');
				}	
				);
				ul.append(li);
			
			});

			$('#linktarget4').html(ul);
		} catch(err) {
			$('#linktarget4').html('Something went wrong!');
		}
})
};

function searchArtist(form) {
	var Artist = form.Artist.value;
	var query = 'PREFIX : <http://www.semanticweb.org/finalproject#> \n PREFIX purl: <http://purl.org/vocab/relationship/> \nSELECT DISTINCT ?x \nWHERE { \n '
	+'{<'+ Artist +'> :hasVibe ?y . \n ?x :hasVibe ?y}\n  UNION {<'+Artist+'> :producerOf ?x} \n UNION {<'+Artist+'> :hasMember ?x} \n UNION {<'+Artist+'> :madeMusicFor ?x} \n UNION {<'+Artist+'> :isMemberOf ?x} '
	+'\n UNION {<'+Artist+'> purl:mentorOf ?x}\n UNION {<'+Artist+'> purl:apprenticeTo ?x} \n}';
	var endpoint = 'http://localhost:5820/Eindprojectu/query';
	var format = 'JSON';
	var reason = 'true';
	console.log(query);

	$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);
		
		try {
			var vars = json.head.vars;
		
			var ul = $('<ul style = "list-style:none;"></ul>');
			ul.addClass('list-group');
		
			$.each(json.results.bindings, function(index,value){
				var li = $('<li></li>');
				li.addClass('list-group-item');
			
				$.each(vars, function(index, v){
					var v_value = value[v]['value'];

					//var myRegexp = /http:\/\/www.semanticweb.org\/finalproject#(.*)/;
					//var match = myRegexp.exec(v_value);
					li.append('<a href = "#" onClick="description(\''+Artist+'\',\''+v_value+'\')">'+v_value+'</a>');
				}	
				);
				ul.append(li);
			
			});

			$('#linktarget4').html(ul);
		} catch(err) {
			$('#linktarget4').html('Something went wrong!');
		}
})
};

function searchSong(form) {
	var Song = form.Song.value;
	var query = 'PREFIX : <http://www.semanticweb.org/finalproject#> \n PREFIX purl: <http://purl.org/vocab/relationship/> \nSELECT DISTINCT ?x \nWHERE { \n '
	+'{:'+ Song +' :hasVibe ?y . \n ?x :hasVibe ?y .\n FILTER (?x != <'+Song+'>) } \n UNION {:'+ Song +' :isGenre ?x } \n UNION {:'+ Song +' :producedBy ?x } \n UNION {:'+ Song +' :isUsedIn ?x } \n}';
	var endpoint = 'http://localhost:5820/Eindprojectu/query';
	var format = 'JSON';
	var reason = 'true';
	var Song2 = 'http://www.semanticweb.org/finalproject#' + Song
	console.log(query);

	$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);
		
		try {
			var vars = json.head.vars;
		
			var ul = $('<ul style = "list-style:none;"></ul>');
			ul.addClass('list-group');
		
			$.each(json.results.bindings, function(index,value){
				var li = $('<li></li>');
				li.addClass('list-group-item');
			
				$.each(vars, function(index, v){
					var v_value = value[v]['value'];

					//var myRegexp = /http:\/\/www.semanticweb.org\/finalproject#(.*)/;
					//var match = myRegexp.exec(v_value);
					li.append('<a href = "#" onClick="description(\''+Song2+'\',\''+v_value+'\')">'+v_value+'</a>');
				}	
				);
				ul.append(li);
			
			});

			$('#linktarget4').html(ul);
		} catch(err) {
			$('#linktarget4').html('Something went wrong!');
		}
})
};


function searchGenre(form) {
	var Genre = form.Genre.value;
	var query = 'PREFIX : <http://www.semanticweb.org/finalproject#> \n PREFIX purl: <http://purl.org/vocab/relationship/> \nSELECT DISTINCT ?x \nWHERE { \n '
	+'<'+ Genre +'> :hasGenre ?x \n }';
	var endpoint = 'http://localhost:5820/Eindprojectu/query';
	var format = 'JSON';
	var reason = 'true';
	var Genre2 = Genre;
	console.log(query);

	$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);
		
		try {
			var vars = json.head.vars;
		
			var ul = $('<ul style = "list-style:none;"></ul>');
			ul.addClass('list-group');
		
			$.each(json.results.bindings, function(index,value){
				var li = $('<li></li>');
				li.addClass('list-group-item');
			
				$.each(vars, function(index, v){
					var v_value = value[v]['value'];

					//var myRegexp = /http:\/\/www.semanticweb.org\/finalproject#(.*)/;
					//var match = myRegexp.exec(v_value);
					li.append('<a href = "#" onClick="description(\''+Genre2+'\',\''+v_value+'\')">'+v_value+'</a>');
				}	
				);
				ul.append(li);
			
			});

			$('#linktarget4').html(ul);
		} catch(err) {
			$('#linktarget4').html('Something went wrong!');
		}
})
};

function searchGame(form) {
	var Game = form.Game.value;
	var query = 'PREFIX : <http://www.semanticweb.org/finalproject#> \n PREFIX purl: <http://purl.org/vocab/relationship/> \nSELECT DISTINCT ?x \nWHERE { \n '
	+'{:'+ Game +' :hasVibe ?y . \n ?x :hasVibe ?y .\n FILTER (?x != :'+Game+') }\n UNION {:'+ Game +' :hasGenre ?x} \n UNION {:'+ Game +' :hasVoiceActor ?x} \n UNION {:'+ Game +' :hasCameoAppearance ?x} \n '
	+ 'UNION {:'+ Game +' :hasMusicFrom ?x} \n UNION {:'+ Game +' :hasType ?x} }';
	var endpoint = 'http://localhost:5820/Eindprojectu/query';
	var format = 'JSON';
	var reason = 'true';
	var Game2 = 'http://www.semanticweb.org/finalproject#'+ Game;
	console.log(query);

	$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);
		
		try {
			var vars = json.head.vars;
		
			var ul = $('<ul style = "list-style:none;"></ul>');
			ul.addClass('list-group');
		
			$.each(json.results.bindings, function(index,value){
				var li = $('<li></li>');
				li.addClass('list-group-item');
			
				$.each(vars, function(index, v){
					var v_value = value[v]['value'];

					//var myRegexp = /http:\/\/www.semanticweb.org\/finalproject#(.*)/;
					//var match = myRegexp.exec(v_value);
					li.append('<a href = "#" onClick="description(\''+Game2+'\',\''+v_value+'\')">'+v_value+'</a>');
				}	
				);
				ul.append(li);
			
			});

			$('#linktarget4').html(ul);
		} catch(err) {
			$('#linktarget4').html('Something went wrong!');
		}
})
};

function searchGameGenre(form) {
	var GameGenre = form.GameGenre.value;
	var query = 'PREFIX : <http://www.semanticweb.org/finalproject#> \n PREFIX purl: <http://purl.org/vocab/relationship/> \nSELECT DISTINCT ?x \nWHERE { \n '
	+':'+ GameGenre +' :isGenre ?x }';
	var endpoint = 'http://localhost:5820/Eindprojectu/query';
	var format = 'JSON';
	var reason = 'true';
	var GameGenre2 = 'http://www.semanticweb.org/finalproject#'+ GameGenre;
	console.log(query);

	$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);
		
		try {
			var vars = json.head.vars;
		
			var ul = $('<ul style = "list-style:none;"></ul>');
			ul.addClass('list-group');
		
			$.each(json.results.bindings, function(index,value){
				var li = $('<li></li>');
				li.addClass('list-group-item');
			
				$.each(vars, function(index, v){
					var v_value = value[v]['value'];

					//var myRegexp = /http:\/\/www.semanticweb.org\/finalproject#(.*)/;
					//var match = myRegexp.exec(v_value);
					li.append('<a href = "#" onClick="description(\''+GameGenre2+'\',\''+v_value+'\')">'+v_value+'</a>');
				}	
				);
				ul.append(li);
			
			});

			$('#linktarget4').html(ul);
		} catch(err) {
			$('#linktarget4').html('Something went wrong!');
		}
})
};

function searchGameType(form) {
	var GameType = form.GameType.value;
	var query = 'PREFIX : <http://www.semanticweb.org/finalproject#> \n PREFIX purl: <http://purl.org/vocab/relationship/> \nSELECT DISTINCT ?x \nWHERE { \n '
	+':'+ GameType +' :isTypeOfGame ?x }';
	var endpoint = 'http://localhost:5820/Eindprojectu/query';
	var format = 'JSON';
	var reason = 'true';
	var GameType2 = 'http://www.semanticweb.org/finalproject#'+ GameType;
	console.log(query);

	$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);
		
		try {
			var vars = json.head.vars;
		
			var ul = $('<ul style = "list-style:none;"></ul>');
			ul.addClass('list-group');
		
			$.each(json.results.bindings, function(index,value){
				var li = $('<li></li>');
				li.addClass('list-group-item');
			
				$.each(vars, function(index, v){
					var v_value = value[v]['value'];

					//var myRegexp = /http:\/\/www.semanticweb.org\/finalproject#(.*)/;
					//var match = myRegexp.exec(v_value);
					li.append('<a href = "#" onClick="description(\''+GameType2+'\',\''+v_value+'\')">'+v_value+'</a>');
				}	
				);
				ul.append(li);
			
			});

			$('#linktarget4').html(ul);
		} catch(err) {
			$('#linktarget4').html('Something went wrong!');
		}
})
};

function description(chosen, result) {
	var query = 'PREFIX : <http://www.semanticweb.org/finalproject#> \nSELECT DISTINCT ?x WHERE { \n {<'+chosen+'> ?x <'+result+'>} \n UNION { <'+chosen+'> :hasVibe ?x . \n<'+result+'> :hasVibe ?x} \n}';
	var endpoint = 'http://localhost:5820/Eindprojectu/query';
	var format = 'JSON';
	var reason = 'true';
	console.log(query);

	$.get('/sparql',data={'endpoint': endpoint, 'query': query, 'format': format, 'reason': reason}, function(json){
		console.log(json);
		
		try {
			var vars = json.head.vars;
		
			var ul = $('<ul style = "list-style:none;"></ul>');
			ul.addClass('list-group');
		
			$.each(json.results.bindings, function(index,value){
				var li = $('<li></li>');
				li.addClass('list-group-item');
			
				$.each(vars, function(index, v){
					var v_value = value[v]['value'];
					console.log(v_value)

					//var myRegexp = /http:\/\/www.semanticweb.org\/finalproject#(.*)/;
					//var match = myRegexp.exec(v_value);
					li.append(v_value);
				}	
				);
				ul.append(li);
			
			});

			$('#linktarget5').html(ul);
		} catch(err) {
			$('#linktarget5').html('Something went wrong!');
		}
})
};
